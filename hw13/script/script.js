const btn = document.querySelector('.theme');
const element = document.querySelector('.style');

let changeTheme = () => {
    if (element.getAttribute('href') === "./style/light.css") {
        element.setAttribute('href', './style/dark.css')
        localStorage.setItem('href', './style/dark.css');
    }else{
        element.setAttribute('href', "./style/light.css");
        localStorage.setItem('href', './style/light.css');

    }
}

//Проверяем есть ли вообще тема и если нету то задаем светлую
if (localStorage.getItem('href') === null) {
    localStorage.setItem('href', './style/light.css');
    console.log(localStorage.getItem('href'))
// если чтото записано по ключу href то мы его перезаписываем
// (когда перезапускаешь страницу и выбрана темная или светлая тема останется та которая была выбрана) 
} else {
    const style = localStorage.getItem('href');
    element.setAttribute('href', `${style}`);
}
console.log(localStorage.getItem('href') === null);

btn.addEventListener('click', changeTheme);

